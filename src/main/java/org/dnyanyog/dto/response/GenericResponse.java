package org.dnyanyog.dto.response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenericResponse {
	private String status;
	private String message;
	@Autowired
	private FriendData information;
	
	
	
	
	

	public FriendData getInformation() {
		return information;
	}
	public void setInformation(FriendData information) {
		this.information = information;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	

}
