package org.dnyanyog.controller;

import org.dnyanyog.dto.request.CreateFriendRequest;
import org.dnyanyog.dto.response.GenericResponse;
import org.dnyanyog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class FriendController {
	@Autowired
	UserService userService;
	
	@PostMapping(path = "friends/api/v1/create" , produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE},consumes= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<GenericResponse> createFriend(@RequestBody CreateFriendRequest request) {
		
		return userService.createFriend(request);
	}
}
