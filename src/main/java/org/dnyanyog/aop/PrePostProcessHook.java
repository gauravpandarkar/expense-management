package org.dnyanyog.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.dnyanyog.entity.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class PrePostProcessHook {
	private static final Logger logger=LoggerFactory.getLogger(PrePostProcessHook.class);
	@After("execution(* org.dnyanyog.repository.*.save(..))")
	public void afterExecution(JoinPoint joinPoint) {
		System.out.println("\n ********************* Saved object-"+joinPoint.getArgs()[0]);
		Users users=(Users)joinPoint.getArgs()[0];
		System.out.println(users.getCountry());
		System.out.println(users.getCurrency());

		System.out.println(users.getEmail());

		System.out.println(users.getFullName());

		System.out.println(users.getLanguage());

		System.out.println(users.getLanguage());
		System.out.println(users.getMobileNo());
		System.out.println(users.getPassword());
		logger.info(users.getCountry());
		logger.info(users.getCurrency());
		logger.info(users.getEmail());
		logger.info(users.getFullName());
		logger.info(users.getMobileNo());
		logger.info(users.getPassword());

		

	}
	@Before("execution(* org.dnyanyog.repository.*.save(..))")
	public void beforeExecution(JoinPoint joinPoint) {
		System.out.println("\n ********************* Saving object-"+joinPoint.getArgs()[0]);
		Users users=(Users)joinPoint.getArgs()[0];
		System.out.println(users.getCountry());
		System.out.println(users.getCurrency());

		System.out.println(users.getEmail());

		System.out.println(users.getFullName());

		System.out.println(users.getLanguage());

		System.out.println(users.getLanguage());
		System.out.println(users.getMobileNo());
		if(users.getPassword().length()<12) {
			System.out.println("Password is less than 12  characters");
		}
		logger.info(users.getCountry());
		logger.info(users.getCurrency());
		logger.info(users.getEmail());
		logger.info(users.getFullName());
		logger.info(users.getMobileNo());
		logger.info(users.getPassword());

	}

}
