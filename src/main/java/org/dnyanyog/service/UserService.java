package org.dnyanyog.service;

import java.util.ArrayList;
import java.util.List;

import org.dnyanyog.dto.request.CreateFriendRequest;
import org.dnyanyog.dto.request.GroupRequest;
import org.dnyanyog.dto.request.LoginRequest;
import org.dnyanyog.dto.request.MappingRequest;
import org.dnyanyog.dto.request.SignUpRequest;
import org.dnyanyog.dto.response.FriendData;
import org.dnyanyog.dto.response.GenericResponse;
import org.dnyanyog.dto.response.GetUserData;
import org.dnyanyog.dto.response.GetUserResponse;
import org.dnyanyog.dto.response.GroupData;
import org.dnyanyog.dto.response.GroupResponse;
import org.dnyanyog.dto.response.LoginResponse;
import org.dnyanyog.dto.response.MappingResponse;
import org.dnyanyog.dto.response.SignUpResponse;
import org.dnyanyog.dto.response.UserData;
import org.dnyanyog.dto.response.UserFails;
import org.dnyanyog.entity.GroupUserMap;
import org.dnyanyog.entity.GroupsInformation;
import org.dnyanyog.entity.Users;
import org.dnyanyog.repository.GroupRepository;
import org.dnyanyog.repository.GroupUserMapRepository;
import org.dnyanyog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	@Autowired
	GroupRepository groupRepository;
	@Autowired
	Users user;
	@Autowired
	GroupsInformation group;
	@Autowired
	SignUpResponse response;
	@Autowired
	SignUpResponse friendResponse;
	@Autowired
	LoginResponse loginResponse;
	@Autowired
	LoginResponse loginResponseFails;
	@Autowired
	UserFails fails;
	@Autowired
	GenericResponse genericResponse;
	@Autowired
	GroupResponse groupResponse;
	@Autowired
	MappingResponse mappingResponse;
	@Autowired
	GroupUserMap groupUserMap;
	@Autowired
	GroupUserMapRepository mapRepository;
	@Autowired
	List<Users> users;
	@Autowired
	GetUserResponse getUserResponse;

	public ResponseEntity<SignUpResponse> saveData(SignUpRequest request) {
		response = new SignUpResponse();
		response.setData(new UserData());
		user = new Users();
		
		if (null != userRepository.findByEmail(request.getEmail())
				&& null != userRepository.findByEmail(request.getEmail()).getPassword())
			return getConflictSignUpResponse();

		if (null != userRepository.findByMobileNo(request.getMobileNo())
				&& null != userRepository.findByMobileNo(request.getMobileNo()).getPassword())
			return getConflictSignUpResponse();

		if (null != userRepository.findByEmail(request.getEmail())
				&& null == userRepository.findByEmail(request.getEmail()).getPassword()) {
			user = userRepository.findByEmail(request.getEmail());
		}

		if (null != userRepository.findByMobileNo(request.getMobileNo())
				&& null == userRepository.findByMobileNo(request.getMobileNo()).getPassword()) {
			user = userRepository.findByMobileNo(request.getMobileNo());
		}
		

		user.setCurrency(request.getCurrency());
		user.setCountry(request.getCountry());
		user.setFullName(request.getFullName());
		user.setEmail(request.getEmail());
		user.setLanguage(request.getLanguage());
		user.setMobileNo(request.getMobileNo());
		user.setPassword(request.getPassword());

		user = userRepository.save(user);
		response.setStatus("success");
		response.setMessage("user account created successfully");
		response.getData().setUserId(user.getUserId());
		response.getData().setCountry(user.getCountry());
		response.getData().setCurrency(user.getCurrency());
		response.getData().setEmail(user.getEmail());
		// response.getData().setFullName(user.getFullName());
		response.getData().setLanguage(user.getLanguage());
		response.getData().setMobile(user.getMobileNo());
		response.getData().setFullName(user.getFullName());

		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	public void saveGroup(GroupRequest request) {

		group.setGroupName(request.getGroupName());
		group.setGroupType(request.getGroupType());
		group.setGroup_Id(request.getGroupId());
		groupRepository.save(group);
	}

	public ResponseEntity<LoginResponse> login(LoginRequest request) {
		loginResponse = new LoginResponse();
		loginResponse.setData(new UserData());
		if (null != userRepository.findByEmail(request.getEmail())
				&& null != userRepository.findByMobileNo(request.getMobile())
				&& null != userRepository.findByPassword(request.getPassword())) {
			user = new Users();
			user.setEmail(request.getEmail());
			user.setMobileNo(request.getMobile());
			user.setPassword(request.getPassword());

			user = userRepository.findByEmail(request.getEmail());
			user = userRepository.findByMobileNo(request.getMobile());
			user = userRepository.findByPassword(request.getPassword());
			loginResponse.setStatus("success");
			loginResponse.setMessage("Validation successfull");
			loginResponse.getData().setUserId(user.getUserId());
			loginResponse.getData().setCountry(user.getCountry());
			loginResponse.getData().setCurrency(user.getCurrency());
			loginResponse.getData().setEmail(user.getEmail());
			loginResponse.getData().setFullName(user.getFullName());
			loginResponse.getData().setLanguage(user.getLanguage());
			loginResponse.getData().setMobile(user.getMobileNo());

			loginResponse.setErrors(null);

			return ResponseEntity.status(HttpStatus.CREATED).body(loginResponse);

		}
		loginResponseFails.setStatus("error");
		loginResponseFails.setMessage("Validation failed");
		loginResponseFails.setData(null);

		loginResponseFails.getErrors().setField("email");
		loginResponseFails.getErrors().setMessage("Email is not valid");
		loginResponseFails.getErrors().setMessage("mobile");
		loginResponseFails.getErrors().setMessage("Mobile number is required");

		return ResponseEntity.status(HttpStatus.CONFLICT).body(loginResponseFails);

	}

	public ResponseEntity<GenericResponse> createFriend(CreateFriendRequest request) {
		genericResponse = new GenericResponse();
		genericResponse.setInformation(new FriendData());

		if (null != userRepository.findByEmail(request.getEmail())
				|| null != userRepository.findByMobileNo(request.getMobile())
				|| null != userRepository.findByFullName(request.getFullName())) {
			genericResponse.setStatus("error");
			genericResponse.setMessage("fullName,email or mobilde already exit");
			genericResponse.setInformation(null);
			;
			return ResponseEntity.status(HttpStatus.CONFLICT).body(genericResponse);
		}

		user = new Users();

		user.setFullName(request.getFullName());
		user.setEmail(request.getEmail());
		user.setMobileNo(request.getMobile());

		user = userRepository.save(user);
		genericResponse.setStatus("success");
		genericResponse.setMessage("User account created successfully");
		genericResponse.getInformation().setUserId(user.getUserId());

		genericResponse.getInformation().setFullName(user.getFullName());
		genericResponse.getInformation().setCountry(user.getCountry());
		genericResponse.getInformation().setCurrency(user.getCurrency());
		genericResponse.getInformation().setEmail(user.getEmail());
		genericResponse.getInformation().setLanguage(user.getLanguage());
		genericResponse.getInformation().setMobile(user.getMobileNo());
		System.out.println("****" + genericResponse);
		return ResponseEntity.status(HttpStatus.CREATED).body(genericResponse);

	}

	public ResponseEntity<GroupResponse> createGroup(GroupRequest request) {
		groupResponse = new GroupResponse();
		groupResponse.setData(new GroupData());

		if (null != groupRepository.findByGroupName(request.getGroupName())) {
			groupResponse.setStatus("error");
			groupResponse.setMessage("Group already exit");
			groupResponse.setData(null);
			return ResponseEntity.status(HttpStatus.CONFLICT).body(groupResponse);
		}
		group = new GroupsInformation();
		group.setGroupName(request.getGroupName());
		group.setGroup_Id(request.getGroupId());
		group.setGroupType(request.getGroupType());
		group = groupRepository.save(group);
		groupResponse.setStatus("success");
		groupResponse.setMessage("Group created successfully");
		groupResponse.getData().setGroupId(group.getGroup_Id());
		groupResponse.getData().setGroupName(group.getGroupName());
		groupResponse.getData().setGroupType(group.getGroupType());
		return ResponseEntity.status(HttpStatus.CREATED).body(groupResponse);
	}

	public ResponseEntity<MappingResponse> mappedFriend(MappingRequest request) {
		groupUserMap.setUserId(request.getUserId());
		groupUserMap.setGroupId(request.getGroupId());
		groupUserMap = mapRepository.save(groupUserMap);

		mappingResponse.setStatus("success");
		mappingResponse.setMessage("Friend added successfully to the group");
		mappingResponse.getData().setGroupId(request.getGroupId());
		mappingResponse.getData().setUserId(request.getUserId());
		return ResponseEntity.status(HttpStatus.CREATED).body(mappingResponse);
	}

	public LoginResponse getUsersData() {
		loginResponse.setStatus("success");
		loginResponse.setMessage("Data fetch successfull");
		users.addAll(users);
		userRepository.findAll();
		return loginResponse;

	}

	public GetUserResponse getUser() {
		List<Users> userList = userRepository.findAll();
		List<GetUserData> getUserDataList = new ArrayList<>();

		for (Users user : userList) {

			GetUserData getUserData = new GetUserData();

			getUserData.setEmail(user.getEmail());
			getUserData.setMobile(user.getMobileNo());
			getUserData.setUserId(user.getUserId());
			getUserData.setFullName(user.getFullName());
			getUserData.setCurrency(user.getCurrency());
			getUserData.setLanguage(user.getLanguage());
			getUserDataList.add(getUserData);
		}

		getUserResponse.setStatus("Success");
		getUserResponse.setMessage("Data fetch successful");
		getUserResponse.setData(getUserDataList);

		return getUserResponse;

	}

	public SignUpResponse getUserById(long userId) {
		user = userRepository.findById(userId).orElse(null);
		// response=new SignUpResponse();
		response.setStatus("success");
		response.setMessage("Data fetch successful");
		response.getData().setUserId(user.getUserId());
		response.getData().setCountry(user.getCountry());
		response.getData().setCurrency(user.getCurrency());
		response.getData().setEmail(user.getEmail());
		response.getData().setFullName(user.getFullName());
		response.getData().setLanguage(user.getLanguage());
		response.getData().setMobile(user.getMobileNo());

		return response;
	}

	private ResponseEntity<SignUpResponse> getConflictSignUpResponse() {
		SignUpResponse response = new SignUpResponse();
		response.setStatus("error");
		response.setMessage("Email or mobile number already registered");
		response.setData(null);

		return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
	}

}
