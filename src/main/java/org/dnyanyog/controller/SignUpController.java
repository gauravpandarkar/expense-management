package org.dnyanyog.controller;

import org.dnyanyog.dto.request.SignUpRequest;
import org.dnyanyog.dto.response.SignUpResponse;
import org.dnyanyog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class SignUpController {
	
	@Autowired
	UserService userService;
	
	@PostMapping(path = "directory/api/v1/signup" , produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE},consumes= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<SignUpResponse> signup(@RequestBody SignUpRequest request) {
		return userService.saveData(request);

	}

}
