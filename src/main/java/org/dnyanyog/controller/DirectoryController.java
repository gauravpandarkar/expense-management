package org.dnyanyog.controller;

import java.util.List;

import org.dnyanyog.dto.request.CreateFriendRequest;
import org.dnyanyog.dto.request.LoginRequest;
import org.dnyanyog.dto.request.SignUpRequest;
import org.dnyanyog.dto.response.GenericResponse;
import org.dnyanyog.dto.response.GetUserResponse;
import org.dnyanyog.dto.response.LoginResponse;
import org.dnyanyog.dto.response.SignUpResponse;
import org.dnyanyog.entity.Users;
import org.dnyanyog.repository.UserRepository;
import org.dnyanyog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DirectoryController {
	@Autowired
	UserService userService;
	@Autowired
	UserRepository userRepository;
	@Autowired 
	GetUserResponse getUserResponse;
	@Autowired
	LoginResponse loginResponse;
	@Autowired 
	List<Users> users;
	
	

	
	@GetMapping(path="directory/api/v1/user/{userId}")
	public SignUpResponse getUserById(@PathVariable long userId) {
		return userService.getUserById(userId);
	}
	@GetMapping(path="directory/api/v1/user")
	public  GetUserResponse getUser() {
		 return userService.getUser();
	}
	
	

}
